<?php
/**
 * Class for creating a tabbed site options page
 * Extends the cmb2-metatabs-options library
 */

namespace Theme;

class SiteOptions {

	/**
	 * Key used for database key and prefixes
	 * @var string
	 */
	protected $options_key = 'site_options';

	/**
	 * Constructor function
	 * Hooks into cmb2_admin_init to create page
	 */
	public function __construct(){
		add_action( 'cmb2_admin_init', [$this, 'initialize'] );
	}

	/**
	 * Creates instance of Cmb2_Metatabs_Options with parameters
	 * defined within this class.
	 */
	public function initialize(){

		$this->social_options_main_page();
		$this->header_options_page();
		$this->footer_options_page();
		$this->tracking_options_page();

	}

	public function social_options_main_page(){

		/**
		 * Registers main options page menu item and form.
		 */
		$args = [
			'id'           => $this->options_key . '_page',
			'title'        => 'Site Options',
			'object_types' => [ 'options-page' ],
			'option_key'   => $this->options_key,
			'tab_group'    => $this->options_key,
			'tab_title'    => 'General',
		];

		$options = new_cmb2_box( $args );
		/**
		 * Options fields ids only need
		 * to be unique within this box.
		 * Prefix is not needed.
		 */

		/**
		 * API Keys
		 */

		$options->add_field([
			'id' => 'api_title',
			'name' => 'API Keys',
			'type' => 'title'
		]);

		$options->add_field([
			'id' => 'GOOGLE_MAPS_GEOCODER_KEY',
			'name' => 'Google Maps Geocoder API Key',
			'type' => 'text',
			'desc' => 'Required for converting addresses to lat/lng for display on maps'
		]);

		$options->add_field([
			'id' => 'GOOGLE_MAPS_JS_KEY',
			'name' => 'Google Maps Geocoder API Key',
			'type' => 'text',
			'desc' => 'Required to display maps'
		]);

		/**
		 * City Options
		 */

		$options->add_field([
			'id'   => 'city_options_title',
			'name' => 'City Info',
			'type' => 'title'
		]);

		$options->add_field([
			'id'   => 'city_name',
			'name' => 'City Name',
			'type' => 'text',
			'desc' => 'This will be used throughout the site'
		]);

		$options->add_field([
			'id'   => 'state_abbrev',
			'name' => 'State Appreviation',
			'type' => 'text',
			'desc' => 'Eg, "CA"'
		]);

		$options->add_field([
			'id'   => 'city_lat',
			'name' => 'City Center Latitude',
			'type' => 'text',
			'desc' => 'Find coordinates here: <a target="_blank" href="https://www.mapdevelopers.com/geocode_tool.php">https://www.mapdevelopers.com/geocode_tool.php</a>'
		]);

		$options->add_field([
			'id'   => 'city_lng',
			'name' => 'City Center Longitude',
			'type' => 'text',
			'desc' => 'Find coordinates here: <a target="_blank" href="https://www.mapdevelopers.com/geocode_tool.php">https://www.mapdevelopers.com/geocode_tool.php</a>'
		]);

		/**
		 * Social Media & Contact Info
		 */

		$options->add_field([
			'id' => 'social_media_title',
			'name' => 'Social Media & Contact Info',
			'type' => 'title'
		]);

		$options->add_field([
			'name' => 'Public Email Address',
			'desc' => 'Your organization\'s public email address',
			'id'   => 'email',
			'type' => 'text',
		]);

		$options->add_field( [
			'name' => 'Facebook Page URL',
			'desc' => 'Insert your Facebook URL here i.e. (https://facebook.com/example/)',
			'id'   => 'facebook_url',
			'type' => 'text_url',
		] );

		$options->add_field( [
			'name' => 'Twitter Profile URL',
			'desc' => 'Insert your Twitter URL here (i.e. https://twitter.com/example/)',
			'id'   => 'twitter_url',
			'type' => 'text_url',
		] );

	}

	public function tracking_options_page(){

		/**
		 * Registers secondary options page, and set main item as parent.
		 */
		$suffix = '_tracking';

		$args = [
			'id'           => $this->options_key . '_page' . $suffix,
			'title'        => 'Tracking Options',
			'menu_title'   => 'Tracking Options', // Use menu title, & not title to hide main h2.
			'object_types' => [ 'options-page' ],
			'option_key'   => $this->options_key . $suffix,
			'parent_slug'  => $this->options_key,
			'tab_group'    => $this->options_key,
			'tab_title'    => 'Tracking',
		];

		$options = new_cmb2_box( $args );

		$options->add_field( [
			'name'    => 'After opening &lt;head&gt;',
			'desc'    => 'This code will be output immediately after the opening &lt;head&gt; tag.',
			'id'      => 'after_opening_head',
			'type'    => 'textarea_code',
		] );

		$options->add_field( [
			'name'    => 'Before closing &lt;/head&gt;',
			'desc'    => 'This code will be output immediately before the closing &lt;/head&gt; tag.',
			'id'      => 'before_closing_head',
			'type'    => 'textarea_code',
		] );

		$options->add_field( [
			'name'    => 'After opening &lt;body&gt;',
			'desc'    => 'This code will be output immediately after the opening &lt;body&gt; tag.',
			'id'      => 'after_opening_body',
			'type'    => 'textarea_code',
		] );

		$options->add_field( [
			'name'    => 'Before closing &lt;/body&gt;',
			'desc'    => 'This code will be output immediately before the closing &lt;/body&gt; tag.',
			'id'      => 'before_closing_body',
			'type'    => 'textarea_code',
		] );

	}

	public function header_options_page(){

		/**
		 * Registers header options page, and set main item as parent.
		 */
		$suffix = '_header';

		$args = [
			'id'           => $this->options_key . '_page' . $suffix,
			'title'        => 'Header Options',
			'menu_title'   => 'Header Options', // Use menu title, & not title to hide main h2.
			'object_types' => [ 'options-page' ],
			'option_key'   => $this->options_key . $suffix,
			'parent_slug'  => $this->options_key,
			'tab_group'    => $this->options_key,
			'tab_title'    => 'Header',
		];

		$options = new_cmb2_box( $args );

		$options->add_field([
			'id' => 'header_identity',
			'name' => 'Identity',
			'type' => 'title'
		]);

		$options->add_field( [
			'name'    => 'Site Logo',
			'id'      => 'logo',
			'type'    => 'file',
			'options' => [
				'url' => false
			]
		] );

		$options->add_field([
			'name' => 'Header Tagline',
			'id' => 'tagline',
			'type' => 'text'
		]);

		$options->add_field([
			'id' => 'header_alert',
			'name' => 'Header Alert',
			'type' => 'title',
			'desc' => 'This will appear at the top of every page.  If you don\'t want one to appear, just delete the content from these fields'
		]);

		$options->add_field([
			'name' => 'Alert Text',
			'id' => 'alert_text',
			'type' => 'textarea_small'
		]);

		$options->add_field([
			'name' => 'Alert Link Label',
			'id' => 'alert_link_label',
			'type' => 'text'
		]);

		$options->add_field([
			'name' => 'Alert Link URL',
			'id' => 'alert_link_url',
			'type' => 'text'
		]);

	}

	public function footer_options_page(){

		/**
		 * Registers footer options page, and set main item as parent.
		 */
		$suffix = '_footer';

		$args = [
			'id'           => $this->options_key . '_page' . $suffix,
			'title'        => 'Footer Options',
			'menu_title'   => 'Footer Options', // Use menu title, & not title to hide main h2.
			'object_types' => [ 'options-page' ],
			'option_key'   => $this->options_key . $suffix,
			'parent_slug'  => $this->options_key,
			'tab_group'    => $this->options_key,
			'tab_title'    => 'Footer',
		];

		$options = new_cmb2_box( $args );

		$options->add_field( [
			'name'    => 'Footer Content',
			'id'      => 'footer_content',
			'type'    => 'wysiwyg',
			'options' => [
				'textarea_rows' => 5
			]
		] );

	}

}